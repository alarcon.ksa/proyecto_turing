package turing_fdi;

import java.awt.Color;
import java.io.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import net.datastructures.*;

public class Turing {

	List<String> cinta;
	List<String> flecha;
	List<String> alfa;
	String[][] matriz;
	String datosMatriz[] = null;
	List<String> auxMatriz = new ArrayList<String>();
	int fila = 0;
	int columna = 0;
	int posicion = 0;
	int posFlecha = 0;
	int auxFlecha = 0;
	int velocidad = 3000;

	public Turing() {
		super();

		cinta = new ArrayList<String>();
		flecha = new ArrayList<String>();
		alfa = new ArrayList<String>();

	}
	
	/*
	 * Metodo que abre el archivo de texto y lo carga en matrices y vectores correpondientes.
	 */

	public void abrirArchivo(String direccionArchivo) {

		try {
			BufferedReader in = new BufferedReader(new FileReader(direccionArchivo));
			String str;
			Boolean esDatos = false;
			Boolean esTabla = false;
			Boolean esCinta = false;

			str = in.readLine();

			while (str != null) {

				// DATOS
				if (str.equals("#datos:")) {
					esDatos = true;
					str = in.readLine();
				}
				if (esDatos) {
					String[] ar = str.split(",");
					for (int i = 0; i < ar.length; i++) {

						alfa.add(alfa.size(), ar[i]);
					}
					str = in.readLine();
				}

				// MATRIZ
				if (str.equals("#tabla:")) {
					esDatos = false;
					esTabla = true;
					str = in.readLine();

					// recorre la matriz del archivo para buscar la cantidad de filas

					while (!str.equals("#cinta:")) {
						datosMatriz = str.split(",");
						for (int i = 0; i < datosMatriz.length; i++) {
							auxMatriz.add(auxMatriz.size(), datosMatriz[i]);
						}

						fila++;
						str = in.readLine();
					}

					// crea la matriz con la cantidad de filas que dio el while y el largo del
					// vector del alfabeto
					matriz = new String[fila][alfa.size()];
					fila = 0;

				}

				if (esTabla) {

					cargarMatriz();

				}
				// CINTA
				if (str.equals("#cinta:")) {
					esTabla = false;
					esCinta = true;
					str = in.readLine();
				}

				if (esCinta) {

					char[] ar = str.toCharArray();
					int auxLength = ar.length * 5;
					for (int i = 0; i < auxLength; i++) {

						if (i < ar.length)
							cinta.add(i, String.valueOf(ar[i]));
						else
							cinta.add(i, "-");
					}

					str = in.readLine();

				}

				if (str.equals("#posicion:")) {
					esCinta = false;
					str = in.readLine();

					for (int i = 0; i < cinta.size(); i++) {
						if (i == Integer.parseInt(str)) {
							flecha.add(flecha.size(), "^");
							posicion = i;
						} else
							flecha.add(flecha.size(), "-");
					}
				}

				str = in.readLine();

			}

			in.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	/*
	 * Metodo que carga la matriz interna
	 */

	public void cargarMatriz() {

		fila = 0;
		columna = 0;
		int aux = 0;

		for (int i = 0; i < auxMatriz.size(); i++) {

			if (aux >= matriz[fila].length) {
				fila++;
				columna = 0;
				aux = 0;
			}

			matriz[fila][columna] = auxMatriz.get(i);
			columna++;
			aux++;

		}

	}
	
	/*
	 * Metodo va llenando el area grafica de cinta, puntero y descripcion.
	 */

	public void iniciarMaquina(JTable tabla, JTextArea detalles) {

		Runnable miRunnable = new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub

				try {

					int row = 0;

					int auxCinta = posicion;

					boolean finCinta = false;
					
					boolean pertenece = false;

					DefaultTableModel dft = (DefaultTableModel) tabla.getModel();
					tabla.setModel(dft);
					detalles.setText("Se inicio apuntando al elemento --> " + cinta.get(auxCinta) + "\n");

					while (!finCinta) {
						
						pertenece = false;

						for (int j = 0; j < alfa.size(); j++) {

							if (cinta.get(auxCinta).equals(alfa.get(j))) {
								
								pertenece = true;
								String datos = matriz[row][j];

								if (datos.equals("error")) {
									detalles.setText(detalles.getText() + "El programa finalizó con errores");
									JOptionPane.showMessageDialog(null, "La cadena contiene errores",
											"Mensaje de error", JOptionPane.ERROR_MESSAGE);
									finCinta = true;

									break;
								}
								if (datos.equals("acepta")) {
									detalles.setText(detalles.getText() + "El programa finalizó con éxito");
									JOptionPane.showMessageDialog(null, "La cadena fue aceptada", "Aceptado",
											JOptionPane.INFORMATION_MESSAGE);
									finCinta = true;

									break;
								} else {

									String[] ar = datos.split("/");

									row = Integer.parseInt(ar[0]);

									for (int l = 0; l < flecha.size(); l++) {
										if (flecha.get(l).equals("^") && ar[1].equals("R")) {

											posFlecha = l;
											auxFlecha = 1;
											break;
										}
										if (flecha.get(l).equals("^") && ar[1].equals("L")) {
											posFlecha = l;
											auxFlecha = -1;
											break;
										}
									}

									flecha.set(posFlecha, "-");
									flecha.set(posFlecha + auxFlecha, "^");

									dft.setValueAt("-", 1, posFlecha);

									int auxif = posFlecha + auxFlecha;

									dft.setValueAt("^", 1, auxif);

									if (cinta.get(auxCinta).equals("-") && !ar[2].equals("lambda")) {

										cinta.set(auxCinta, ar[2]);
										detalles.setText(detalles.getText() + "Se añadio el nuevo elemento --> "
												+ cinta.get(auxCinta) + "\n");

										dft.setValueAt(ar[2], 0, posFlecha);

									} else {
										if (!ar[2].equals("lambda")) {
											cinta.set(auxCinta, ar[2]);
											dft.setValueAt(ar[2], 0, posFlecha);
										}
									}

								} // Fin else

								if (auxFlecha == 1)
									detalles.setText(detalles.getText()
											+ "La flecha se movio a la derecha y apunta al elemento --> "
											+ cinta.get(auxCinta + 1) + "\n");
								else
									detalles.setText(detalles.getText()
											+ "La flecha se movio a la izquierda y apunta al elemento --> "
											+ cinta.get(auxCinta + 1) + "\n");

								dft.fireTableDataChanged();

								Thread.sleep(velocidad);

							} // Fin If
							

						} // Fin For
						
						if (!pertenece) {
							
							detalles.setText(detalles.getText() + "El programa finalizó con errores");
							JOptionPane.showMessageDialog(null, "La cadena contiene caracteres que no pertenecen al alfabeto",
									"Mensaje de error", JOptionPane.ERROR_MESSAGE);
							finCinta = true;
							break;
							
						}//Fin if

						auxCinta++;

					} // Fin While

				} catch (Exception e) {
					e.printStackTrace();
				} // Fin Catch

			}// Fin Run
		}; // Fin Runnable
		Thread hilo = new Thread(miRunnable);
		hilo.start();

	}// Fin metodo

	/*
	 * Metodo que imprime el alfabeto almacenado en un vector, a partir de lo leído
	 * en el archivo .txt y lo muestra en una linea de texto de forma grafica
	 */
	public void imprimirAlfabeto(JTextField alfa) {

		alfa.setBackground(Color.ORANGE);

		String aux = " ";

		for (int i = 0; i < this.getAlfa().size(); i++)
			aux += this.getAlfa().get(i) + "\t";

		alfa.setText(aux);
	}

	/*
	 * Metodo que imprime la matriz que se llenó a partir del .txt y la muestra en
	 * un area de texto grafica
	 */

	public void imprimirMatriz(JTextArea mat) {

		mat.setBackground(Color.PINK);

		for (int i = 0; i < matriz.length; i++) {

			for (int j = 0; j < matriz[i].length; j++) {

				mat.append(matriz[i][j]);
				mat.append("\t");
				mat.setBounds(mat.getX(), mat.getY(), mat.getWidth() + 5, mat.getHeight());

			}

			mat.append("\n");

			mat.setBounds(mat.getX(), mat.getY(), mat.getWidth(), mat.getHeight() + 5);

		}
	}

	/*
	 * Metodo que vacia todos los contenedores, matrices y vectores, reposiciona
	 * toda la estructura grafica que se encuentre por debajo de la matriz y vuelve
	 * todas las variables a su estado inicial.
	 */

	public void reiniciarVentana(JPanel p, JTextField dir, JTextField alfa, JTextArea mat, JLabel cint,
			JScrollPane panel, JTable tab, JButton Iniciar, JButton Detener, JButton SubirV, JButton BajarV,
			JLabel Descrip, JScrollPane jsdetalle, JTextArea detallesta) {

		boolean cintaVacia = false;
		boolean alfaVacio = false;
		boolean flechaVacia = false;
		int aux = 0;

		dir.setText("");
		alfa.setText("");
		alfa.setBackground(Color.white);
		alfa.setSize(360, 27);
		mat.setText("");
		mat.setBackground(Color.WHITE);
		mat.setSize(356, 97);
		cint.setBounds(160, 276, 82, 20);
		panel.setBounds(140, 307, 530, 70);
		DefaultTableModel dtm = new DefaultTableModel();
		tab.setModel(dtm);
		tab.setBounds(70, 307, 430, 44);

		while (cintaVacia == false || alfaVacio == false || flechaVacia == false) {

			if (!this.getCinta().isEmpty())
				this.getCinta().remove(aux);
			else
				cintaVacia = true;

			if (!this.getAlfa().isEmpty())
				this.getAlfa().remove(aux);
			else
				alfaVacio = true;

			if (!this.getFlecha().isEmpty())
				this.getFlecha().remove(aux);
			else
				flechaVacia = true;

		}

		Iniciar.setBounds(548, 399, 108, 37);
		Detener.setBounds(680, 399, 108, 37);
		SubirV.setBounds(260, 402, 139, 30);
		BajarV.setBounds(101, 402, 139, 30);

		Descrip.setBounds(160, 461, 145, 20);
		jsdetalle.setBounds(140, 492, 519, 58);
		detallesta.setText(null);

		auxMatriz = new ArrayList<String>();

		matriz = new String[0][0];

		fila = 0;
		posFlecha = 0;
		auxFlecha = 0;

		velocidad = 3000;

		p.revalidate();

	}

	public List<String> getCinta() {
		return cinta;
	}

	public List<String> getFlecha() {
		return flecha;
	}

	public List<String> getAlfa() {
		return alfa;
	}

	public String[][] getMatriz() {
		return matriz;
	}

	public void setMatriz(String[][] matriz) {
		this.matriz = matriz;
	}

	public void setCinta(List<String> cinta) {
		this.cinta = cinta;
	}

	public void setFlecha(List<String> flecha) {
		this.flecha = flecha;
	}

	public int getVelocidad() {
		return velocidad;
	}

	public void setVelocidad(int velocidad) {
		this.velocidad = velocidad;
	}

}
