package turing_fdi;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.border.LineBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.Color;
import javax.swing.border.BevelBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;

import javax.swing.Action;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JTextPane;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.LayoutManager;

import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import java.awt.Component;
import javax.swing.ScrollPaneConstants;

public class Turing_front extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private Turing maqTur;
	private final Action action = new SwingAction();
	private JTextField textField;
	private JTextField textFieldAlfa;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Turing_front frame = new Turing_front();
					frame.setVisible(true);
					frame.setTitle("Maquina de Turing");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Turing_front() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(80, 80, 903, 650);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(230, 230, 250));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);

		maqTur = new Turing();

		JLabel dirTxtLabel = new JLabel("Ubicaci\u00F3n del archivo:");
		dirTxtLabel.setBounds(20, 21, 220, 20);
		contentPane.add(dirTxtLabel);

		JTextField direccion = new JTextField();
		direccion.setBounds(160, 21, 285, 20);
		contentPane.add(direccion);
		direccion.setColumns(10);
		direccion.setEditable(false);

		JTextArea matriztextArea = new JTextArea();
		matriztextArea.setBounds(140, 168, 356, 97);
		contentPane.add(matriztextArea);
		matriztextArea.setEditable(false);

		JButton btnExaminar = new JButton("Examinar");
		btnExaminar.setBounds(471, 20, 89, 23);
		contentPane.add(btnExaminar);

		JButton btnCargar = new JButton("Cargar");
		btnCargar.setBounds(571, 20, 89, 23);
		contentPane.add(btnCargar);

		table = new JTable();
		table.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		table.setBounds(140, 307, 430, 44);
		contentPane.add(table);

		JButton btnIniciar = new JButton("INICIAR");
		btnIniciar.setBounds(548, 399, 108, 37);
		contentPane.add(btnIniciar);

		JLabel lblMatriz = new JLabel("---Matriz---");
		lblMatriz.setFont(new Font("Baskerville Old Face", Font.PLAIN, 20));
		lblMatriz.setBounds(160, 137, 82, 20);
		contentPane.add(lblMatriz);

		JLabel lblcinta = new JLabel("---Cinta---");
		lblcinta.setFont(new Font("Baskerville Old Face", Font.PLAIN, 20));
		lblcinta.setBounds(160, 276, 82, 20);
		contentPane.add(lblcinta);

		JLabel lblAlfabeto = new JLabel("---Alfabeto---");
		lblAlfabeto.setFont(new Font("Baskerville Old Face", Font.PLAIN, 20));
		lblAlfabeto.setBounds(160, 68, 145, 20);
		contentPane.add(lblAlfabeto);
	

		JLabel lblDescripcion = new JLabel("---Descripci�n---");
		lblDescripcion.setFont(new Font("Baskerville Old Face", Font.PLAIN, 20));
		lblDescripcion.setBounds(160, 461, 145, 20);
		contentPane.add(lblDescripcion);

		textFieldAlfa = new JTextField();
		textFieldAlfa.setBounds(140, 99, 360, 27);
		contentPane.add(textFieldAlfa);
		textFieldAlfa.setColumns(10);
		textFieldAlfa.setEditable(false);

		JButton btnReiniciar = new JButton("Reiniciar");
		btnReiniciar.setBounds(670, 20, 89, 23);
		contentPane.add(btnReiniciar);

		JScrollPane pane = new JScrollPane(table);
		pane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		pane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		pane.setViewportBorder(new LineBorder(Color.BLACK));
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		pane.setBounds(140, 307, 530, 70);
		table.remove(table.getTableHeader());
		table.setEnabled(false);
		pane.setEnabled(false);
		table.setTableHeader(null);
		contentPane.add(pane);

		JScrollPane scrollPaneDescripcion = new JScrollPane();
		scrollPaneDescripcion.setBounds(140, 492, 519, 58);
		contentPane.add(scrollPaneDescripcion);

		JTextArea textAreaDescripcion = new JTextArea();
		scrollPaneDescripcion.setViewportView(textAreaDescripcion);
		textAreaDescripcion.setEditable(false);

		JButton btnBajarVelocidad = new JButton("Bajar Velocidad");
		btnBajarVelocidad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				maqTur.setVelocidad(maqTur.getVelocidad() + 1000);
				textAreaDescripcion.setText(textAreaDescripcion.getText() + "Se baj� la velocidad de recorrido a "
						+ (maqTur.getVelocidad() / 1000) + " segundos por elemento \n");

			}
		});
		btnBajarVelocidad.setBounds(101, 402, 139, 30);
		contentPane.add(btnBajarVelocidad);

		JButton btnSubirVelocidad = new JButton("Subir Velocidad");
		btnSubirVelocidad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (maqTur.getVelocidad() > 1000)
					maqTur.setVelocidad(maqTur.getVelocidad() - 1000);
				else if (maqTur.getVelocidad() <= 1000 && maqTur.getVelocidad() > 0)
					maqTur.setVelocidad(maqTur.getVelocidad() - 100);
				
				double velSub = (double)maqTur.getVelocidad() / 1000 ;

				textAreaDescripcion.setText(textAreaDescripcion.getText() + "Se subi� la velocidad de recorrido a "
						+ (velSub) + " segundos por elemento \n");
			}
		});
		btnSubirVelocidad.setBounds(260, 402, 139, 30);
		contentPane.add(btnSubirVelocidad);

		JButton btnDetener = new JButton("FINALIZAR");
		btnDetener.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				maqTur.setVelocidad(0000);
			}

		});
		btnDetener.setBounds(680, 399, 108, 37);
		contentPane.add(btnDetener);

		btnReiniciar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				maqTur.reiniciarVentana(contentPane, direccion, textFieldAlfa, matriztextArea, lblcinta, pane, table,
						btnIniciar, btnDetener, btnSubirVelocidad, btnBajarVelocidad, lblDescripcion,
						scrollPaneDescripcion, textAreaDescripcion);

			}
		});

		btnCargar.addActionListener(new ActionListener() {

			@SuppressWarnings("null")
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

				maqTur.imprimirMatriz(matriztextArea);

				textFieldAlfa.setBounds(textFieldAlfa.getX(), textFieldAlfa.getY(), matriztextArea.getWidth(),
						textFieldAlfa.getHeight());
				maqTur.imprimirAlfabeto(textFieldAlfa);

				lblcinta.setBounds(lblcinta.getX(), matriztextArea.getY() + 155, lblcinta.getWidth(),
						lblcinta.getHeight());

				pane.setBounds(matriztextArea.getX(), matriztextArea.getY() + 180, 530, 60);

				table.setBounds(pane.getX(), pane.getY() - 10, matriztextArea.getWidth(), 44);

				btnIniciar.setBounds(btnIniciar.getX(), pane.getY() + 80, btnIniciar.getWidth(),
						btnIniciar.getHeight());
				btnDetener.setBounds(btnDetener.getX(), btnIniciar.getY(), btnDetener.getWidth(),
						btnDetener.getHeight());
				btnSubirVelocidad.setBounds(btnSubirVelocidad.getX(), btnIniciar.getY(), btnSubirVelocidad.getWidth(),
						btnSubirVelocidad.getHeight());
				btnBajarVelocidad.setBounds(btnBajarVelocidad.getX(), btnIniciar.getY(), btnBajarVelocidad.getWidth(),
						btnBajarVelocidad.getHeight());
				
				lblDescripcion.setBounds(lblDescripcion.getX(), btnSubirVelocidad.getY() + 59 , lblDescripcion.getWidth(), lblDescripcion.getHeight());
				scrollPaneDescripcion.setBounds(scrollPaneDescripcion.getX(), btnSubirVelocidad.getY() + 90, scrollPaneDescripcion.getWidth(), scrollPaneDescripcion.getHeight());
				
				DefaultTableModel dtm = new DefaultTableModel();

				String fila_cinta = "";
				String fila_flecha = "";

				for (int i = 0; i < maqTur.getCinta().size(); i++) {

					if (i == 0) {
						fila_cinta = maqTur.getCinta().get(i) + ",";
						fila_flecha = maqTur.getFlecha().get(i) + ",";
					} else {
						fila_cinta += maqTur.getCinta().get(i) + ",";
						fila_flecha += maqTur.getFlecha().get(i) + ",";
					}

				}

				String[] row1 = fila_cinta.split(",");
				String[] row2 = fila_flecha.split(",");

				// se crea la cantidad de columnas
				for (int i = 0; i < row1.length; i++)
					dtm.addColumn(" ");

				// se crean las filas
				dtm.addRow(row1);
				dtm.addRow(row2);

				table.setModel(dtm);

			}

		});

		btnExaminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				JFileChooser fc = new JFileChooser();

				fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
				FileNameExtensionFilter filtro = new FileNameExtensionFilter("Text Files", "txt");
				fc.addChoosableFileFilter(filtro);

				fc.showOpenDialog(fc);

				String ruta = fc.getSelectedFile().getAbsolutePath();
				direccion.setText(ruta);
				maqTur.abrirArchivo(ruta);

			}
		});

		btnIniciar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				maqTur.iniciarMaquina(table, textAreaDescripcion);

			}
		});

	}

	private class SwingAction extends AbstractAction {
		public SwingAction() {
			putValue(NAME, "SwingAction");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}

		public void actionPerformed(ActionEvent e) {
		}
	}
}
